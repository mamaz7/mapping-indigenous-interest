---
description: "Tips on debugging"
layout: default
author: "Marc J. Mazerolle"
date: "2019-06-19"
#keep_md: yes
---


## Tips on debugging
- `R` is case-sensitive. The code to run the tool is 
```
> RunCode( )
```
Not following this convention will result in an error.

- Inspect the names of the variables in your data files carefully so they match those expected as explained in [data preparation section](software-data#prepareData).
