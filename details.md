---
description: "Technical details and contact information"
layout: default
author: "Marc J. Mazerolle"
date: "2019-04-17"
#keep_md: yes
---

## Technical details<a name="technicalDetails"></a>
The approach used with this tool consists of building a series of main effects logistic regressions based on all subset regression (McCullagh and Nelder 1989, Agresti 2002). The predictions are based on the entire model set using multimodel inference (Burnham and Anderson 2002).

### Prior checks
A number of diagnostics are run to eliminate variables that either are constant or that introduce perfect classification. The levels of each variable are checked for consistency across the three data files. The code also enforces to maintain at least 10 observations for each estimated parameter to avoid overfitting. 

### Model building
Logistic regression models are fit with the `glm( )` function in `R`. Each model is screened to ensure convergence following the estimation by maximum likelihood and models for which the algorithm does not converge are eliminated. The variance inflation factor (VIF) is computed for each variable in each model to identify issues of multicollinearity among explanatory variables. Models with VIF > 10 are eliminated.

### Model selection
The models having passed the diagnostics are then compared using model selection based on Akaike's information criterion corrected for small sample size (AIC<sub>c</sub>, Burnham and Anderson 2002). Additional metrics are provided for the top-ranking model, including the evidence ratio of the top-ranking model compared to the null model, Nagelkerke's pseudo R<sup>2</sup>, the percent deviance explained, and a log-likelihood test against the null model. 

### Multimodel inference and predictions
Models with virtually no support (Akaike weights < 0.000001) are excluded from the multimodel inference. Multimodel inference is used to compute model-averaged parameter estimates with the shrinkage estimator to determine the effect of the explanatory variables using 95% confidence intervals (Burnham and Anderson 2002). Multimodel inference is also used to obtain predictions for the sites of interest as well as the percent correct classification. The code will produce plots showing the predictions ± 95% confidence intervals for the variables having an effect on the probability of having a site of aboriginal interest.

### R Packages required<a name="packages"></a>
The functions in the `R` script require packages `tcltk` (included with the default `R` installation), [AICcmodavg](https://CRAN.R-project.org/package=AICcmodavg){:target="_blank"}, [rmarkdown](https://CRAN.R-project.org/package=rmarkdown){:target="_blank"}, and [xtable](https://CRAN.R-project.org/package=xtable){:target="_blank"}, which are all available from the [Comprehensive R Archive Network (CRAN)](https://cran.r-project.org/){:target="_blank"}. These packages will be installed automatically the first time you run the `RunCode( )` function in the `R` console.


### References
> Anderson, D. R. 2008. Model-based inference in the life sciences: a primer on evidence. Springer, New York, USA.

> Agresti, A. 2002. Categorical data analysis. John Wiley & Sons, Inc., Hoboken, New Jersey, USA.

> Burnham, K. P., and D. R. Anderson. 2002. Model selection and multimodel inference: a practical information-theoretic approach, 2nd edition. Springer-Verlag, New York.

> McCullagh, P., and J. A. Nelder. 1989. Generalized linear models, 2nd edition. Chapman & Hall, New York, USA.

<br/>

## Author<a name="author"></a>
This tool was developed and is maintained by [Dr. Marc J. Mazerolle](https://www.sbf.ulaval.ca/professeurs/marc-mazerolle){:target="_blank"} at Université Laval for a project in collaboration with [Dr. Hugo Asselin](https://www.uqat.ca/services/personnel/fiche.asp?Fiche=960026){:target="_blank"} at l'Université du Québec en Abitibi-Témiscamingue.

<img src="Images/logoLaval.jpg" alt="drawing" width="200"/>
<img src="Images/logoUQAT.jpg" alt="drawing" width="200"/>
