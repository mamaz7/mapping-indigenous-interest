---
#title: Home
description: "A site with instructions on using the tool"
layout: default
author: "Marc J. Mazerolle"
date: '2019-04-17'
#keep_md: yes
---

## Welcome

This site includes the documentation, installation files, and step-by-step instructions to use the tool to map sites of aboriginal interest. These instructions will help you get started with mapping areas of interest on the landscape based on modelling with a series of variables.


## Getting started

- Ensure that the required software is installed on your computer. Check the [software requirements](software-data#software).

- Download the code for the tool itself. Follow the [download instructions](software-data#downloadTool).

- Format the data to use with the tool following the [data preparation instructions](software-data#prepareData).

- Take a look at the [typical workflow](workflow#workflow) when using the tool. 

- Run the [example session](example#example) to check that the tool works on your computer, before trying it out with your own data.

- Check the [troubleshooting section](troubleshooting#tips) to solve commonly encountered issues.

- To know more about the underlying statistical approaches behind the tool, check the [Technical details](details#technicalDetails).
