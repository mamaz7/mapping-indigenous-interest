---
title: "Getting started"
author: "Marc J. Mazerolle"
date: '2019-04-17'
output:
html_document:
keep_md: yes
---

<br/>

## A quick overview

These instructions are designed to get started with mapping areas of interest on the landscape based on a series of variables. The tool requires the following software on Windows, Mac or GNU/Linux to be installed before running the code:

- A recent version of `R`, available for download at (https://cran.r-project.org/).

- A recent version of `R Studio Desktop`, available for download at (https://www.rstudio.com/products/rstudio/download/).

- A working version of a Geographic Information System (GIS), such as `QGIS`, available for download at (https://www.qgis.org/en/site/forusers/download.html), or a license of `ArcGIS` (https://desktop.arcgis.com).

<br/>

Using the tool involves the important following steps:

1. extracting polygon information of areas identified as of interest (positives) and saving in a `.csv` file (i.e., comma separated values).

2. extracting polygon information of areas selected randomly (ideally, the same number as positives) and saving in a `.csv` file.

3. running the R script to supply the data, run the models, generate the predictions and save to an external file. 

4. importing the resulting predictions into your Geographic Information System (GIS) of choice (`QGIS`, `ArcGIS`).

These different steps are described in detail in the rest of this document.

<br/>

## 1. Preparing the data --- the file with positives
To use the code, you must prepare a file that consists of the polygons that have been identified as being of interest according to traditional knowledge (see example file [positive.csv](Example/positive.csv)). This file includes a unique identifier for each polygon (e.g., `recno`) as well as a column labeled as 'Positive' which corresponds to 1, indicating that the polygon was identified as an area of interest. This file also includes some of the following variables: `density`, `slope`, `drainage`, `age`, `structure`, `surf_dep`, `road`, `water`, `height`, `forest_cov`, and `keystone`.

To function efficiently, each categorical (group) variable should be limited to two or three levels. Thus, the user should consider pooling certain levels and strive to have a similar number of observations in each group. The file should be saved in `.csv` format (comma-separated values).

<br/>

## 2. Preparing the data --- the file with random polygons
The user must prepare a second file featuring a random selection of polygons among those that have not been identified as being of interest. This second file should include a unique identifier for each polygon (e.g., `recno`) as well as a column labeled `Positive` containing 0's, indicating that each polygon was not identified as being an area of interest. This file must include the same explanatory variables as in the file described above. In addition, the levels of the variables must match in both files to be included in the analysis.

<br/>

## 3. Preparing the data --- the file containing polygons of study area
A third file must also be prepared, this one containing all the polygons for which predictions are requested. This file must have a unique identifier for each polygon (e.g., `recno`) and the same explanatory variables as in the two files mentioned above. These variables must have the same levels that are used in the two files, because it is not possible to make predictions on a level that was not observed in the data used to build the models.

<br/>

## 4. Running the R script
To run the R script, follow these steps:
1. Download the [RScript](RScript) folder that contains the [RunCode.R](RScript/RunCode.R) file. 

2. Start `R Studio` and open the [RunCode.R](RScript/RunCode.R) file. You will see the content of this file in the upper right panel of `R Studio`:

![Capture000](Images/Capture000.png)

3. Move your cursor to the upper right panel by clicking near the top of the text. Click on the `source` icon to send the code to the `R` console. Alternatively, it is also possible to copy the entire content of the file and paste it in the lower left panel (the `R` console).

![Capture00](Images/Capture00.png)

4. Type the following command at the `R` prompt (without typing `>`) in the bottom left panel:

> `> run.code( )`

![Capture0](Images/Capture0.png)

Then, press `enter` or `return` on your keyboard. The function starts by checking that the required `R` packages are installed on the computer. If the required packages are not found, then it will proceed with their installation. Depending on your computer or internet connection, this may take a few minutes to complete the installation. Note that the package installation only occurs at first use, you do not need to reinstall them each time you use the tool.

The user will be prompted to navigate to the folder where the files are stored. All output files from the analysis will be saved in this folder. A series of dialog boxes will then request the user to select the file with positive polygons, a file with random polygons, as well as a file with the entire set of polygons for which a prediction is requested. 

Computation time will vary with the size of the file for which predictions are requested. Upon completion of the script, a confirmation window will appear indicating that the files have been saved. The following results files will be saved to the working directory specified by the user at the beginning of the script:

* the model-averaged predictions are saved in the `predictions.csv` file.

* the model selection table is saved in the `modelSelection.csv` file.

* the model-averaged parameter estimates obtained with the shrinkage estimator are saved in the `modavgBetas.csv` file.

* a summary report file will also be produced and stored in `report.html`

<br/>

## An example session
To run the example, download the contents of the [Example](Example) folder. This example assumes that you have already opened the [RunCode.R](RunCode.R) file in `R Studio` and that you have sourced the contents of the file to the `R` console.

![Capture00](Images/Capture00.png)

1. Type `run.code( )` in the `R` console (i.e., bottom left panel in `R Studio`). 

![Capture0](Images/Capture0.png)


Then press `enter` at the `R` console. 

If the required packages are not present on your computer, they will be installed and will see details on their installation in the `R` console. Once the packages are installed, a window will appear requesting the path to the folder that contains the input files. Click on the folder icon to navigate to the folder containing the files:

![Capture1](Images/Capture1.png)

2. After having located the folder where the data are located, press `OK`. A second window will appear requesting the input file with the positives: 

![Capture2](Images/Capture2.png)

In our example, this is the [positive.csv](Example/positive.csv) file, which is a file in comma-separate value format with a number of variables, but a mandatory `Positive` column. Here is an excerpt of the file:

![positive](Images/positive.png)

The `Positive` column consists exclusively of the number one to indicate that these are the data for the sites that have been identified as being of interest. Locate the file and click `OK`.

3. A third window will then request the input file with the randomly selected sites:

![Capture3](Images/Capture3.png)

In our example, we are using the [random.csv](Example/random.csv) file in our example, which is also in the comma-separated value format. This file should have a structure identical as the file in step 2: it should feature the same variables using the same labels in both files. The file should also include a `Positive` column. This column should contain exclusively zeros for the randomly selected sites, as these sites are among the ones that have not been identified as being of interest.

![random](Images/random.png)

Select the file in the window and click `OK`.

4. A final fourth window will request the file that contains all sites for which predictions are requested:

![Capture4](Images/Capture4.png)

Here, it is the [polygonsForPredictions.csv](Example/polygonsForPredictions.csv) file, also in the comma-separated value format. This file contains the data on the variables of the polygons for which predictions are requested. It is important to note that these variables must have the same levels as those observed in the files for positive polygons and random polygons (in our example, [positive.csv](Example/positive.csv) and [random.csv](Example/random.csv)). It is not possible to make predictions on a level that was not observed in the files used to build the models. Here are the first rows of the file used for prediction in our example:

![prediction](Images/prediction.png)

Select the file and click `OK`. You will then see a window indicating the progression of the prediction process:

![Capture5](Images/Capture5.png)

Note that the time to obtain predictions depends on the total number of polygons for which you want predictions, the number of variables involved in the model building (i.e., number of columns in the `positive.csv` and `random.csv` files), but also on the processing power of your computer. In some cases, this process can take a few hours.

Once the prediction process is completed, a final window will appear indicating the routine was successful:

![Capture6](Images/Capture6.png)

You can then click on `OK`. Note that a number of files, including the modeling results and predictions will be saved in the same directory specified in the first step above.

To have a look at the contents of the results of the example, you can download the [Results](Example/Results) folder, with the files [predictions.csv](Example/Results/predictions.csv), [modelSelection.csv](Example/Results/modelSelection.csv), [modavgBetas.csv](Example/Results/modavgBetas.csv), and [report.html](Example/Results/report.html). Note that [report.html](Example/Results/report.html) summarizes the results of the entire process.

<br/>

## Technical details
The approach used here consists of building a series of main effects logistic regressions based on all subset regression (McCullagh and Nelder 1989, Agresti 2002). Logistic regression models are then compared using model selection based on Akaike's information criterion corrected for small sample size (AIC<sub>c</sub>, Burnham and Anderson 2002). Multimodel inference is then used to compute model-averaged parameter estimates with the shrinkage estimator as well as predictions for the sites of interest (Burnham and Anderson 2002). 

The functions in the `R` script require packages `tcltk`, `AICcmodavg`, `rmarkdown`, and `xtable`, which are all available from the [Comprehensive Archive Network (CRAN)](https://cran.r-project.org/).

### References:
> Anderson, D. R. 2008. Model-based inference in the life sciences: a primer on evidence. Springer, New York, USA.

> Agresti, A. 2002. Categorical data analysis. John Wiley & Sons, Inc., Hoboken, New Jersey, USA.

> Burnham, K. P., and D. R. Anderson. 2002. Model selection and multimodel inference: a practical information-theoretic approach, 2nd edition. Springer-Verlag, New York.

> McCullagh, P., and J. A. Nelder. 1989. Generalized linear models, 2nd edition. Chapman & Hall, New York, USA.
