---
description: "An example session"
layout: default
author: "Marc J. Mazerolle"
date: '2019-06-19'
#keep_md: yes
---


## An example session<a name="example"></a>
Before running the example, download the three files below: 

- [positive.csv](Example/positive.csv)
- [random.csv](Example/random.csv)
- [polygonsForPredictions.csv](Example/polygonsForPredictions.csv)

Save these three files in the same folder on your computer and note down the path leading to the folder. This example assumes that you have already opened the [RunCode.R](RScript/RunCode.R) file in `R Studio` and that you have sourced the contents of the file to the `R` console as described [here](workflow#runScript).


### Running the example
1. Go to the bottom left panel of `R Studio` and click in the `R` console. Type `RunCode( )` at the console followed by `enter` or `return` on your keyboard:
```
> RunCode( )
```
![cap4](Images/cap4.png)
<br/>
If the [required packages](details#packages) are not present on your computer, they will be installed. You will see details on their installation in the `R` console. Note that package installation only occurs the first time you use the tool. Once the packages are installed, a window will appear requesting the path to the folder that contains the input files. 

2. Click on the folder icon to navigate to the folder containing the files:
<br/>
![cap5](Images/cap5.png)
<br/>
After having located this folder, double-click on it and press `OK`. This will set the working directory for the analysis: all input files must be in this folder. The output files produced during the analysis will also be stored in the same folder.

3. Next, a new window will appear requesting the input file with the positives: 
![cap6](Images/cap6.png)
In our example, this is the [positive.csv](Example/positive.csv) file, which is a file in comma-separate value format with a number of variables, but a mandatory `Positive` column. 
<br/>
<br/>
Here is an excerpt of the `positive.csv` file:
![positive](Images/positive.png)
The `Positive` column consists exclusively of the number one to indicate that these are the data for the sites that have been identified as being of interest. Locate the file and click `Open` (or `Ouvrir`, depending on the language of your operating system).

3. A third window will then request the input file with the randomly selected sites:
![cap7](Images/cap7.png)
In our example, we are using the [random.csv](Example/random.csv) file in our example, which is also in the comma-separated value format. This file should have a structure identical as the file in step 2: it should feature the same variables using the same labels in both files. The file should also include a `Positive` column. This column should contain exclusively zeros for the randomly selected sites, as these sites are among the ones that have not been identified as being of interest.
<br/>
<br/>
Here is an excerpt of the `random.csv` file:
![random](Images/random.png)
After having selected the `random.csv` file in the window, click `Open`.

4. A final fourth window will request the file that contains all polygons (sites) for which predictions are requested:
![cap8](Images/cap8.png)
In our example, the [polygonsForPredictions.csv](Example/polygonsForPredictions.csv) file contains this information. The file is also in the comma-separated value format. It is important to note that these variables must have the same levels as those observed in the files for positive polygons and random polygons (in our example, [positive.csv](Example/positive.csv) and [random.csv](Example/random.csv)). It is not possible to make predictions on a level that was not observed in the files used to build the models. 
<br/>
<br/>
Here are the first rows of the `polygonsForPredictions.csv` file :
![prediction](Images/prediction.png)
Select the file and click `Open`. 

5. You will then see a window indicating the progression of the prediction process:
![cap9](Images/cap9.png)
<br/>
Note that the time to obtain predictions depends on the total number of polygons for which you want predictions, the number of variables involved in the model building (i.e., number of columns in the `positive.csv` and `random.csv` files), but also on the processing power of your computer. In some cases, this process can take a few hours. The example takes approximately 9 minutes to run on a computer with an Intel Core i7-6600U CPU @ 2.60GHz × 4 processor on 64 bit Debian GNU/Linux 9 with 16 GB of RAM.

6. Once the prediction process is completed, a final window will appear indicating the routine was successful:
<br/>
![cap10](Images/cap10.png)
<br/>
You can then click on `OK`. 
<br/>
<br/>
The tool will produce output files, including the modeling results and predictions. These files will be saved in the same directory specified in the first step above (i.e., the working directory). 

8. Now that you have run the analysis, you can take a look at the contents of the results of the example in your working directory. You can use the results to create a map of your study area based on the probabilities of each site being of aboriginal interest.

The results of this example are also available in the following files:
- [predictions.csv](Example/Results/predictions.csv)
- [modelSelection.csv](Example/Results/modelSelection.csv)
- [modavgBetas.csv](Example/Results/modavgBetas.csv)
- [report.html](Example/Results/report.html)

Note that [report.html](Example/Results/report.html) summarizes the results of the entire process.


## Running the code with your own data
Now that you've successfully completed the example, you can use the code with your own data. In case you get an error message, you can take a look at the [troubleshooting section](troubleshooting).
