---
description: "Troubleshooting tips"
layout: default
author: "Marc J. Mazerolle"
date: "2019-06-19"
#keep_md: yes
---


## A few helpful tips when encountering problems<a name="tips"></a>
In case you get errors when running the code, here are some potential solutions to solve common issues.

#### R complains about not finding the function. 
**Solution 1:** Check that you [downloaded the file](software-data#downloadTool) and [sourced the function to `R`](workflow#runScript). 

**Solution 2:** `R` is case-sensitive. The code to run the tool is 
```
> RunCode( )
```
Not following this convention will result in an error.


#### R complains about not being able to install the required packages. 

**Solution 1:** Check that you have a working internet connection. 

**Solution 2:** Check that you have a recent version of `R` installed on your computer. If your version is more than 1 year old, you should uninstall the old version and install the most recent one.

**Solution 3:** Some of the packages change over time. If this is the case, contact the [author](details#author) of the tool for additional help. 

#### R stops abruptly after specifying the input files. 

**Solution:** Check that the input files have been formatted correctly as comma-separated files and that the column names match those described in the [data preparation section](software-data#prepareData). Note that the name of the column to indicate a site of interest in your data set should be `Positive`.


#### The tool does not return an error, but is slow and keeps working until I close `R`.

**Solution:** Try the [example session](example#example) first to see how long it runs on your computer. If your data set is very large, it may take several hours to finish the routine. You can also try a subset of your data to see if any problems occur. For example, you could run the code to make predictions on 100 polygons instead of 1000.
