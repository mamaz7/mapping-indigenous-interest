---
description: "Typical workflow"
layout: default
author: "Marc J. Mazerolle"
date: "2019-04-17"
#keep_md: yes
---

## Typical workflow<a name="workflow"></a>

Using the tool involves the following steps:

1. extracting polygon information of areas identified as of interest (positives) and saving in a `.csv` file (i.e., comma separated values).

2. extracting polygon information of areas selected randomly (ideally, the same number as positives) and saving in a `.csv` file.

3. preparing a polygon file on which to make predictions in the study area and saving in a `.csv` file.

4. running the R script to import the data, run the models, generate the predictions and save to an external file. 

5. importing the resulting predictions into your Geographic Information System (GIS) of choice (`QGIS`, `ArcGIS`).

<br/>

## Running the R script<a name="runScript"></a>
To run the R script, follow these steps:
1. Download the <a href="RScript/RunCode.R" download target="_blank">RunCode.R</a> file. 

2. Start `R Studio` and use its menu in the upper left panel to open the [RunCode.R](RScript/RunCode.R) file. You will see the content of this file in the upper right panel of `R Studio`:
![cap1](Images/cap1.png)
3. Move your cursor to the upper right panel by clicking near the top of the text. Click on the `source` icon to send the code of the entire file to the `R` console. Alternatively, it is also possible to copy the entire content of the file and paste it in the lower left panel (the `R` console).
![cap2](Images/cap2.png)
You will see in the `R` console that the code has been sourced:
![cap3](Images/cap3.png)

4. Type the following command at the `R` prompt (without typing `>`) in the bottom left panel:
```
> RunCode( )
```
![cap4](Images/cap4.png)
Then, press the `enter` or `return` key on your keyboard. The function starts by checking that the [required `R` packages](details#packages) are installed on the computer. If the required packages are not found, then it will proceed with their installation. Depending on your computer or internet connection, this may take a few minutes to complete the installation. Note that the package installation only occurs at first use, you do not need to reinstall them each time you use the tool.

5. Once the packages have been installed, the user will be prompted to navigate to the folder where the files are stored. All output files from the analysis will be saved in this folder. 
![cap5](Images/cap5.png)

6. A series of dialog boxes will then request the user to select the file with positive polygons, a file with random polygons, as well as a file with the entire set of polygons for which a prediction is requested:
![cap6](Images/cap6.png)
![cap7](Images/cap7.png)
![cap8](Images/cap8.png)

7. A window indicating the time until completion will appear next:
![cap9](Images/cap9.png)
<br/>
Computation time will vary with the size of the file for which predictions are requested. 

8. Upon completion of the script, a confirmation window will appear indicating that the files have been saved.
<br/>
![cap10](Images/cap10.png)

The tool will produce the following results files and save these to the working directory specified by the user at the beginning of the script:
* the model-averaged predictions are saved in the `predictions.csv` file.
* the model selection table is saved in the `modelSelection.csv` file.
* the model-averaged parameter estimates obtained with the shrinkage estimator are saved in the `modavgBetas.csv` file.
* a summary report file will also be produced and stored in `report.html`

<br/>

## Time to try out the example
Now that you have an idea of the steps to follow to use the tool, continue to the [detailed tutorial](example.html) with an example data set.
