---
description: "Software and data preparation"
layout: default
author: "Marc J. Mazerolle"
date: "2019-04-17"
#keep_md: yes
---

## Software requirements<a name="software"></a>

The tool requires the following software on Windows, Mac or GNU/Linux to be installed before running the code:

- A recent version of `R`, available for download at [the Comprehensive R Archive Network (CRAN)](https://cran.r-project.org/){:target="_blank"}.

- A recent version of `R Studio Desktop`, available for download at [www.rstudio.com](https://www.rstudio.com/products/rstudio/download/){:target="_blank"}.

- A working version of a Geographic Information System (GIS), such as `QGIS`, available for download at [www.qgis.org](https://www.qgis.org/en/site/forusers/download.html){:target="_blank"}, or a license of `ArcGIS` [www.desktop.arcgis.com](https://desktop.arcgis.com){:target="_blank"}.

<br/>

## Downloading the tool<a name="downloadTool"></a>
Download the <a href="RScript/RunCode.R" download target="_blank">RunCode.R</a> file that contains the code for the tool. Save the file on your computer and note the folder where it is located. The first time you run the code on the computer, the tool will install the [`R` packages required](details#packages). 

<br/>

## Data preparation<a name="prepareData"></a>
Three files are required by the tool. These files should be prepared according to the instructions below.

### 1. The file with positives<a name="preparePositive"></a>
The file with positives consists of the polygons that have been identified as being of interest according to traditional knowledge (see example file [positive.csv](Example/positive.csv)). This file should include a unique identifier for each polygon (e.g., `recno`) as well as a column labeled as `Positive` containing the value 1 for each row, indicating that the polygon was identified as an area of interest. This file also includes some of the following variables: `density`, `slope`, `drainage`, `age`, `structure`, `surf_dep`, `road`, `water`, `height`, `forest_cov`, and `keystone`.

To function efficiently, each categorical (group) variable should be limited to two or three levels. Thus, the user should consider pooling certain levels and strive to have a similar number of observations in each group defined by the categorical variable. The file with positives should be saved in `.csv` format (comma-separated values).

### 2. The file with random polygons<a name="prepareRandom"></a>
The file with random polygons features a random selection of polygons among those that have not been identified as being of interest. This second file should include a unique identifier for each polygon (e.g., `recno`) as well as a column labeled `Positive` containing 0's, indicating that each polygon was not identified as being an area of interest. See the example file [random.csv](Example/random.csv). **This file must include the same explanatory variables and column names as in the file described above. In addition, the levels of the variables must match in both files to be included in the analysis.**

### 3. The file with polygons for which predictions are requested<a name="prepareAll"></a>
The third file contains all the polygons for which predictions are requested. This file must have a unique identifier for each polygon (e.g., `recno`) and the same explanatory variables as in the two files mentioned above (see example file [polygonsForPredictions.csv](Example/polygonsForPredictions.csv)). **This file must include the same explanatory variables, column names and levels, as those used in the two files above. Indeed, the tool does not allow making predictions on a level that was not observed in the data used to build the models.**

<br/>

## Ready to use the tool
Now that your data are organized in the three files as instructed above, you can continue to the next section on the [typical workflow](workflow.html) when working with the tool.
