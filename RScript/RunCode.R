############################
############################
##Tool to predict areas of interest from GIS data
##
##Author: M. J. Mazerolle, U. Laval
##Date:  12 June 2019
##
##INSTRUCTIONS:
##1) open the file in RStudio
##2) click on source to send entire contents of file to R console
##
##For detailed instructions, https://gitlab.com/mamaz7/mapping-indigenous-interest/
############################
############################



###########
##importing data
RunCode <- function( ) {
  
    ##packages to install if not already installed
    list.of.packages <- c("tcltk", "AICcmodavg", "rmarkdown", "xtable")
    new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
    if(length(new.packages) > 0) install.packages(new.packages, repos = "http://cran.utstat.utoronto.ca/")
    
    ##load tcltk package
    require(tcltk)

    ##prompts the user to choose working directory where files are located
    ##setwd("/home/mazerolm/Documents/Stats consulting/Aboriginal GIS tool/fichiers_texte")
    setwd(dir = tclvalue(tkchooseDirectory(title = "Choose the working directory where all files are located")))
    
    
    ##import data polygons with attributes of interest
    ##pos <- read.table("positive.csv", header = TRUE, sep = ",")
    pos <- read.table(file = tk_choose.files(default = "", caption = "Select input file with positives (csv format)",
                                             multi = FALSE, filters = NULL, index = 1),
                      header = TRUE, sep = ",")
    
    ##import data polygons with randomly selected sites
    ##rand <- read.table("random.csv", header = TRUE, sep = ";")
    rand <- read.table(file = tk_choose.files(default = "",
                                              caption = "Select input file with randoms (csv format)",
                                              multi = FALSE, filters = NULL, index = 1),
                       header = TRUE, sep = ",")
    
    ##import data polygons for predictions
###for.prediction <- read.table("all_polygons.csv", header = TRUE, sep = ",")
    for.prediction <- read.table(file = tk_choose.files(default = "",
                                                        caption = "Select input file of polygons for which you want predictions (csv format)",
                                                        multi = FALSE, filters = NULL, index = 1),
                                 header = TRUE, sep = ",")
    
    ##pool of names
    total.names <- c("density", "slope", "drainage", "age", "structure", "surf_dep",
                     "road", "water", "height", "forest_cov", "keystone", "recno", "Positive")

    ##explanatory variable names
    var.names <- c("density", "slope", "drainage", "age", "structure", "surf_dep",
                   "road", "water", "height", "forest_cov", "keystone")

    ##extract names for positive
    pos.names <- sort(names(pos))

    ##extract names for random
    rand.names <- sort(names(rand))

    ##check for same names in both positives and random
    if(!identical(pos.names, rand.names)) stop("\ncolumn names must be identical for both files, please change accordingly\n")
    
    ##select names
    pos.names.sel <- pos.names[pos.names %in% total.names]
    rand.names.sel <- rand.names[rand.names %in% total.names]
    
    ##combine both data sets
    combo.orig <- rbind(pos[, pos.names.sel], rand[, rand.names.sel])
    combo <- combo.orig

    ##extract names of variables
    combo.names <- names(combo)
    ##identify explanatory variables
    combo.names.expl <- combo.names[combo.names %in% var.names]

    ##number of variables
    n.vars <- length(combo.names.expl)

    ##check for constant variables and perfect classification
    out.constant <- rep(NA, n.vars)
    out.perfect <- rep(NA, n.vars)

    ##iterate over each variable
    for (i in 1:n.vars) {
        explan.tmp <- combo[, combo.names.expl[i]]
        constant <- length(table(explan.tmp)) <= 1
        groups <- as.data.frame(table(explan.tmp, combo$Positive))
        perfect.class <- any(groups$Freq == 0)
        
        ##variable constant or not
        out.constant[i] <- constant
        out.perfect[i] <- perfect.class
    }
    
    ##idenfify variables that are constant or with perfect classification
    if(any(out.constant) | any(out.perfect)) {
        const.vars <- combo.names.expl[out.constant]
        perf.vars <- combo.names.expl[out.perfect]
        
        ##constants
        if(any(out.constant)){
            if(length(const.vars) == 1) {
                cat("\nThe variable \"", paste(const.vars), "\" is a constant and was removed from analysis\n")
            } else {
                cat("\nThe following variables are constants and were removed from analysis\n",
                    paste(const.vars, collapse = ", "), "\n\n")
            }
        }
        
        ##perfect classification
        if(any(out.perfect)) {
            if(length(perf.vars) == 1) {
                cat("\nThe variable \"", paste(perf.vars), "\" has perfect classification and was removed from analysis\n")
            } else {
                cat("\nThe following variables have perfect classification and were removed from analysis\n",
                    paste(perf.vars, collapse = ", "), "\n\n")
            }
        }
    
        ##remove variables that are constants or with perfect classification
        combo.names.expl <- combo.names.expl[!out.constant & !out.perfect]
        ##update the names of the variables
        combo <- combo[, c("Positive", combo.names.expl)]
    }
  

    ##check variable names and factor levels in data frame for predictions
    ##factor levels
    fact.levels <- list( )
    for (i in combo.names.expl) {
        fact.levels[[i]] <- levels(combo[, i])
    }
    
    ##factor levels
    fact.levels.pred <- list( )
    for (i in combo.names.expl) {
        fact.levels.pred[[i]] <- levels(for.prediction[, i])
    }
    
    
    ##check that both have identical levels
    common.levels <- rep(NA, length(combo.names.expl))
    names(common.levels) <- combo.names.expl
    for(i in combo.names.expl) {
        common.levels[i] <- identical(fact.levels[i], fact.levels.pred[i])
    }
    
    problem.variables.id <- which(!common.levels)
    problem.variables <- combo.names.expl[problem.variables.id]
    
    ##check if there is problem with prediction data set
    if(length(problem.variables) > 0) {
        combo.names.expl <- combo.names.expl[-problem.variables.id]
        ##update the names of the variables
        combo <- combo[, c("Positive", combo.names.expl)]
        cat("\nSome variables have different levels in the model-building and prediction data sets.\n",
            "The following variables have been removed: ", problem.variables, "\n")
    }


    ##extract columns of explanatory variables
    just.explanatory <- combo[, combo.names.expl]
    
    ##number of explanatory variables remaining
    n.explan <- ncol(just.explanatory)

    ##CHECK NUMBER OF PARAMETERS WITH LEVELS USED
    ##formula
    form.explanatory <- paste("~", paste(names(just.explanatory), collapse = " + "))
    modMat <- model.matrix(data = just.explanatory, as.formula(form.explanatory))
    nparms <- ncol(modMat)
    
    ##determine total sample size
    nobs <- nrow(combo)
    
    ##maximum number of variables in any given model
    maxK <- round(nobs/10, digits = 0)
    
    ##determine number of variables to put in the model (given sample size and number of usable variables)
    ##fewer usable variables than max allowable
    if(nparms <= maxK) {
        usedK <- nparms
    }
    
    ##more usable variables than max allowable
    if(nparms > maxK) {
        usedK <- maxK
    }
 

    ##GO DIRECTLY TO COMPUTATION OF ALL COMBINATIONS
    
    ##1) determine number of potential variables
    ##2) determine number of maximum parameters allowed given sample size
    ##3) check models with a given number of parameters <= (maxK or n.generic, whichever is smallest)/2
    ##4) compute AICc and Akaike weights
    ##5) provide the top models in output (those with delta AICc < 2), model-averaged estimates of important variables,
    ##   and model-averaged predictions for entire study area
    
    ##other issues: potential correlation between variables
    
    ##identify the cases with a specified number of variables in models
    ##pool of 11 variables
    combs <- do.call(expand.grid, rep(list(c(FALSE, TRUE)), n.explan))[-1, ] #remove intercept
    
    ##number of variables in each model
    check.sums <- rowSums(combs)
    
    ##select models with K <= usedK
    #combs.sel <- combs[which(check.sums <= usedK), ]
    
    ##build model formulas for all possible combination
    form.vars <- apply(combs, 1, function(i) names(just.explanatory)[i])
    form.vars.All <- paste(" ~ ", lapply(form.vars, paste, collapse=" + "), sep = "")
    ##extract model matrix
    modMatAll <- lapply(form.vars.All, FUN = function(i) model.matrix(data = just.explanatory, as.formula(i)))
    ##count number of parameters and identify which are <= usedK
    estK <- sapply(modMatAll, FUN = ncol)

    ##determine models with <= usedK
    idK <- which(estK <= usedK)
    form.vars.Sub <- form.vars.All[idK]
    form.var.raw <- paste("Positive", lapply(form.vars.Sub, paste, collapse=" + "), sep = "")
    ##add null model
    form.var.raw.null <- c(form.var.raw, "Positive ~ 1")
    form.var.nice <- lapply(form.var.raw.null, as.formula)
    
    ##run models
    cat("\nRunning logistic regression models ... \n\n")
    
    model.list <- lapply(form.var.nice, function(x) eval(substitute(glm(x,
                                                                        data=combo, family = binomial), list(x=x, data=combo, glm))))
    cat("\nDone.\n\n")
    
    ##identify models that did not converge
    convergence <- unlist(lapply(model.list, function(x) x$converged))
    model.list.converged <- model.list[convergence]
    
    ##reject models that have a high VIF
    ##identify models with high VIF
    vif.out <- lapply(model.list.converged, FUN = vif)
    vif.result <- lapply(vif.out, FUN = function(i) i$result) 
    vif.id <- unlist(lapply(vif.out, FUN = function(i) i$vif.vals))
    model.list.clean <- model.list.converged[which(!vif.id)]

    ##assign names to each model
    names(model.list.clean) <- unlist(lapply(model.list.clean, FUN = formula))
    
    ##compute AICc table
    cat("\nComputing model selection table ... \n\n")
    
    require(AICcmodavg)
    mod.sel.table <- aictab(cand.set = model.list.clean)

    cat("\nDone.\n\n")
    
    ##compute model diagnostics
    model.diagnostics <- modelDiag(model.table = mod.sel.table, model.list = model.list.clean)
     
    ##for printing, identify models with delta AICc < 4
    simplified.table <- mod.sel.table[mod.sel.table$Delta_AICc <= 4,
                                      c("Modnames", "K", "AICc", "Delta_AICc", "AICcWt", "LL", "Cum.Wt")]
    
    ##rownames(simplified.table) <- simplified.table$Modnames
    names(simplified.table)[1] <- "Model.names"
    ##round table
    simplified.table$AICc <- round(simplified.table$AICc, digits = 4)
    simplified.table$Delta_AICc <- round(simplified.table$Delta_AICc, digits = 4)
    simplified.table$AICcWt <- round(simplified.table$AICcWt, digits = 4)
    simplified.table$LL <- round(simplified.table$LL, digits = 4)
    simplified.table$Cum.Wt <- round(simplified.table$Cum.Wt, digits = 4)
    names(simplified.table)[6] <- "log-likelihood"
    names(simplified.table)[7] <- "Cumulative.weight"

    ##remove extra column
    ##  simplified.table <- simplified.table[, c("K", "AICc", "Delta_AICc", "AICcWt", "LL", "Cum.Wt")]
  
    ##identify models with AICcWt < 0.000001 (1e-06); 2048 * 1e-06 = 0.002048 (error of 0.002)
    ##remove models with virtually no weight
    short.table <- mod.sel.table[which(mod.sel.table$AICcWt > 1e-06),]
    ##order model names
    short.table <- short.table[order(short.table$Modnames), ]
    
    ##create short list of candidate models
    short.list <- model.list.clean[as.character(short.table$Modnames)]
    ##order lists by model names
    short.list <- short.list[sort(names(short.list))]
    
    ##to speed up computations, determine all observed combinations
    cat("\nIdentifying unique combinations of explanatory variables in data set ... \n\n")
  
    ##create function to determine observed
    all.combs <- apply(for.prediction[, combo.names.expl], MARGIN = 1, FUN = function(x) paste(x, collapse = "."))
    ##add pasted values in a new column
    for.prediction$all.combs <- all.combs
    unique.combs <- unique(all.combs)
    ##label for combinations
    label.combs <- paste(combo.names.expl, collapse = ".")

    ##select a single occurrence of each combination
    select.comb <- list( )
    for(new in 1:length(unique.combs)) {
        select.comb[[new]] <- for.prediction[for.prediction$all.combs == unique.combs[new], ][1, ]
    }

    ##convert to data.frame
    shortCombo <- do.call(rbind, select.comb)
    
    cat("\nDone.\n\n")

    ##compute model-averaged predictions - based on short list (excludes models with AICcWt < 1e-06)
    ##system.time(out.pred <- modavgPred(cand.set = short.list, newdata = shortCombo,
    ##                                   type = "response"))
    out.predList <- vector("list", length = nrow(shortCombo))
    
    ##progress bar here
    pb <- tkProgressBar(title = "Computing model-averaged predictions",
                        label = "% done", min = 0, max = nrow(shortCombo), initial = 0)

    ##iterate over each of the variable combinations that occur in the original data
    ##this saves a lot of computing time
    for(i in 1:nrow(shortCombo)) {
        out.predList[[i]] <- modavgPred(cand.set = short.list, newdata = shortCombo[i, ],
                                        type = "response")
        
        ## update progress bar
        pc <- round((i/nrow(shortCombo))*100) #percent done
        info <- sprintf("%d%% done", pc)
        setTkProgressBar(pb, i, sprintf("Computing model-averaged predictions (%s)", info), info)
    }

    ##close connection
    close(pb)
    
    ##combine in matrix
    out.pred <- t(sapply(out.predList, FUN = function(i) i$matrix.output))
    colnames(out.pred) <- c("mod.avg.pred", "uncond.se", "lower.CL", "upper.CL")
    
    ##extract all.combs and fit
    shortCombo$predicted.probability <- round(out.pred[, "mod.avg.pred"], digits = 4)

    ##merge combinations of variables above (reduced data set) with for.prediction (full data set)
    probPredict <- merge(shortCombo[, c("all.combs", "predicted.probability")], for.prediction)
    

########################################### CHANGE HERE
    ##compute model-averaged predictions for correct classification - based on training data set
    ##training data set is the one combining positive and random polygons
    ##streamline predictions for training data set
    combo.origTrain <- combo.orig

    ##create function to determine observed
    all.combsTrain <- apply(combo.origTrain[, combo.names.expl], MARGIN = 1, FUN = function(x) paste(x, collapse = "."))
    ##add pasted values in a new column
    combo.origTrain$all.combsTrain <- all.combsTrain
    unique.combsTrain <- unique(all.combsTrain)
    ##label for combinations
    label.combsTrain <- paste(combo.names.expl, collapse = ".")

    ##select a single occurrence of each combination
    select.combTrain <- list( )
    for(new in 1:length(unique.combsTrain)) {
        select.combTrain[[new]] <- combo.origTrain[combo.origTrain$all.combsTrain == unique.combsTrain[new], ][1, ]
    }

    ##convert to data.frame
    shortComboTrain <- do.call(rbind, select.combTrain)

    cat("\nDone.\n\n")

    ##compute model-averaged predictions - based on short list (excludes models with AICcWt < 1e-06)
    out.predListTrain <- vector("list", length = nrow(shortComboTrain))

    ##progress bar here
    pb <- tkProgressBar(title = "Computing model-averaged predictions on training data set",
                        label = "% done", min = 0, max = nrow(shortComboTrain), initial = 0)

    ##iterate over each of the variable combinations that occur in the original data
    ##this saves a lot of computing time
    for(i in 1:nrow(shortComboTrain)) {
        out.predListTrain[[i]] <- modavgPred(cand.set = short.list, newdata = shortComboTrain[i, ],
                                             type = "response")
        
        ## update progress bar
        pc <- round((i/nrow(shortComboTrain))*100) #percent done
        info <- sprintf("%d%% done", pc)
        setTkProgressBar(pb, i, sprintf("Computing model-averaged predictions on training data set (%s)", info), info)
    }

    ##close connection
    close(pb)
    
    ##combine in matrix
    out.predTrain <- t(sapply(out.predListTrain, FUN = function(i) i$matrix.output))
    colnames(out.predTrain) <- c("mod.avg.pred", "uncond.se", "lower.CL", "upper.CL")
    
    ##extract all.combs and fit
    shortComboTrain$fit <- round(out.predTrain[, "mod.avg.pred"], digits = 4)

    ##merge combinations of variables above (reduced data set) with for.prediction (full data set)
    probPredictTrain <- merge(shortComboTrain[, c("all.combsTrain", "fit")], combo.origTrain)

    ##compute table for correct classification
    probPredictTrain$fit0.5 <- ifelse(probPredictTrain$fit < 0.5, 0, 1)
    predCTable <- with(data = probPredictTrain,
                       table(fit0.5, Positive))
    ##true negatives
    TN <- predCTable[1, 1]
    ##true positives
    TP <- predCTable[2, 2]
    ##model-averaged accuracy (percent correct classification using threshold of 0.5)
    Acc <- (TN + TP)/nrow(probPredictTrain)
        
    ##save predictions in a new file
    cat("\nExporting output files ...\n\n")
  
    write.table(x = probPredict[, c("recno", "predicted.probability")], file = "predictions.csv", row.names = FALSE,
                col.names = TRUE, sep = ",")
    cat("\nFile \"predictions.csv\" was saved in ", getwd( ), "\n\n")

    ##save model selection table in a new file
    write.table(x = simplified.table[order(simplified.table$Delta_AICc), ],
                file = "modelSelection.csv", row.names = FALSE,
                col.names = TRUE, sep = ",")
    cat("\nFile \"modelSelection.csv\" was saved in ", getwd( ), "\n\n")

    ##compute model-averaged estimates of each parameter

    ##extract all estimates
    all.estimates <- unique(unlist(lapply(model.list.clean, FUN = function(i) names(coef(i)))))
    nEstimates <- length(all.estimates)
    shrinkageList <- list( )

    ##progress bar here
    pb <- tkProgressBar(title = "Computing model-averaged coefficients",
                        label = "% done", min = 0, max = nEstimates, initial = 0)
    
    for(i in 1:nEstimates) {
        shrinkageList[[i]] <- modavgShrink(cand.set = model.list.clean, parm = all.estimates[i])
        
        ## update progress bar
        pc <- round((i/nEstimates)*100) #percent done
        info <- sprintf("%d%% done", pc)
        setTkProgressBar(pb, i, sprintf("Computing model-averaged coefficients (%s)", info), info)
    }

    ##close connection
    close(pb)

    ##extract output
    shrinkName <- sapply(shrinkageList, FUN = function(i) i$Parameter)
    shrinkEst <- sapply(shrinkageList, FUN = function(i) i$Mod.avg.beta)
    shrinkSE <- sapply(shrinkageList, FUN = function(i) i$Uncond.SE)
    shrinkLow95 <- sapply(shrinkageList, FUN = function(i) i$Lower.CL)
    shrinkUpp95 <- sapply(shrinkageList, FUN = function(i) i$Upper.CL)

    ##determine which CI's exclude 0
    exclude.zero <- ifelse(shrinkLow95 < 0 & shrinkUpp95 < 0, "neg",
                    ifelse(shrinkLow95 > 0 & shrinkUpp95 > 0, "pos",
                           NA))
    
    ##arrange in matrix
    shrinkMat <- matrix(data = c(shrinkEst, shrinkSE, shrinkLow95, shrinkUpp95),
                        nrow = length(shrinkageList), ncol = 4)
    colnames(shrinkMat) <- c("Mod.avg.beta", "Uncond.SE", "Lower95", "Upper95")
    rownames(shrinkMat) <- shrinkName
    shrinkFrame <- cbind(Parameter = shrinkName, as.data.frame(round(shrinkMat, 4)))
    
    write.table(shrinkFrame, file = "modavgBetas.csv",
                col.names = TRUE, row.names = FALSE, sep = ",",
                quote = FALSE)
    
    cat("\nFile \"modavgBetas.csv\" was saved in ", getwd( ), "\n\n")

    
    cat("\nGenerating html report to save in", getwd( ), "\n\n")

    ##generate html report
    dynamic.report(just.explanatory,
                   combo,
                   pos,
                   rand,
                   combo.names.expl,
                   short.table,
                   short.list,
                   model.diagnostics,
                   shrinkMat,
                   Acc)

    ##add successful completion message box
    tkmessageBox(title = "Finished",
                 message = "Routine completed successfully. You can now import the \"predictions.csv\" file in ArcGIS or QGIS.",
                 icon = "info", type = "ok")

    
}



##create dynamic report in html from output
dynamic.report <- function(just.explanatory,
                           combo,
                           pos,
                           rand,
                           combo.names.expl,
                           short.table,
                           short.list,
                           model.diagnostics,
                           shrinkMat,
                           Acc) {

    require(rmarkdown)
    require(xtable)

mdownObj <- c(
'# Report of landscape elements of aboriginal interest',
'(code executed on `r date( )`)',
'<br/>',
'---',
'',
'The main portions of the output resulting from the analysis of the data are illustrated below.',
'',
'<br/>',
'',
'```{r results = "asis", echo = FALSE}',
'##create summary table of each factor',
'##for each variable, check levels',
'varLevels <- lapply(just.explanatory, levels)',
'##number of levels',
'numLevels <- sapply(varLevels, FUN = length)',
'```',
'',
'Number of observations used to build model:  `r nrow(combo)` (`r nrow(pos)` positives, `r nrow(rand)` random).',
'',
'Variables used to build the models: `r combo.names.expl`.',
'',
'These variables had between `r min(numLevels)` and `r max(numLevels)` levels (Table 1).',
'',
'```{r results = "asis", echo = FALSE}',
'outLevels <- lapply(varLevels, FUN = function(i) paste(i, collapse = ", "))',
'##store in vector',
'outLevelsMat <- unlist(outLevels)',
'outLevelsFrame <- data.frame(Variable = names(outLevelsMat))',
'outLevelsFrame$Levels <- outLevelsMat',
'print(xtable(outLevelsFrame,',
'caption = "Table 1. Explanatory variables and corresponding levels used to build models."), type = "html",',
'include.rownames = FALSE, caption.placement = "top")',
'```',
'',
'<br/>',
'',
'##Result of model building and model selection',
'',
'```{r results = "asis", echo = FALSE}',
'##model selection table',
'shortTable <- short.table[order(short.table$Delta_AICc), ]',
'print(xtable(shortTable[shortTable$Delta_AICc < 4, ],',
'caption = "Table 2. Model selection table (models ranked based on AIC~c~) showing the ranking of the different models used with the training data set."), type = "html",',
'include.rownames = FALSE, caption.placement = "top")',
'',
'##extract formula name in top model',
'formTop <- gsub(pattern = " ", replacement = "",',
'x = unlist(strsplit(gsub(x = as.character(shortTable$Modnames[1]), pattern = "Positive ~", replacement = ""),',
'split = "\\\\+")))',
'',
'##check for model diagnostics',
'LRout <- ifelse(model.diagnostics$Pvalue < 1 & model.diagnostics$Pvalue >= 0.0001,',
'model.diagnostics$Pvalue, "< 0.0001")',
'LRsign <- ifelse(model.diagnostics$Pvalue <= 0.05, "yes", "no")',
'LRprint <- ifelse(LRsign == "yes", "is better", "is not better")',
'LRpaste <- ifelse(LRout == "< 0.0001", "P < 0.0001", paste("P = ", model.diagnostics$Pvalue))',
'```',
'<br/>',
'',
'The top-ranked model has `r 100*round(shortTable[1, "AICcWt"], 3)`% of the support and consists of the variables `r formTop[-length(formTop)]`, and `r formTop[length(formTop)]` (Table 2).  The top-ranked model explains `r round(model.diagnostics$DevianceExplained, 3)*100`% of the deviance and has a Nagelkerke pseudo R<sup>2</sup> of `r round(model.diagnostics$Nagel.R2, digits = 2)`. This model is `r formatC(model.diagnostics$evidenceRatio, 3)` times more parsimonious than the null model consisting only of the intercept. The likelihood-ratio test indicates that the top-ranked model `r LRprint` than the null model (log-likelihood ratio test = `r round(model.diagnostics$LR, digits = 4)`, df = `r model.diagnostics$DF`, `r LRpaste`). Overall, the model set correctly classifies the positives and negatives `r 100*round(Acc, 3)`% of the time based on the data used to build the models.',
'',
'',
'',
'##Inspection of the variables used to make predictions',
'```{r results = "asis", echo = FALSE}',
'niceCoef <- round(shrinkMat[-1, ], digits = 4)',
'colnames(niceCoef) <- c("Estimate", "SE", "Lower 95% limit", "Upper 95% limit")',
'',
'##identify variables that have an effect',
'effCoef <- ifelse(niceCoef[, 3] > 0 & niceCoef[, 4] > 0, "pos",',
'ifelse(niceCoef[, 3] < 0 & niceCoef[, 4] < 0, "neg", "none"))',
'##remove variables without effect',
'effCoefOnly <- effCoef[effCoef != "none"]',
'effCoefOnlyNames <- names(effCoefOnly)',
'neffCoefOnly <- length(effCoefOnly)',
'',
'##iterate to identify variables involved',
'holdVar <- matrix(NA, nrow = length(combo.names.expl),',
'ncol = neffCoefOnly)',
'rownames(holdVar) <- combo.names.expl',
'colnames(holdVar) <- effCoefOnlyNames',
'for(i in 1:length(combo.names.expl)) {',
'for(j in 1:neffCoefOnly) {	     ',
'holdVar[i, j] <- regexpr(pattern = combo.names.expl[i], text = effCoefOnlyNames[j])',
'}',
'}',
'mainVarNames <- names(which(apply(holdVar, 1, FUN = function(i) any(i == 1))))',
'NmainVarNames <- length(mainVarNames)',
'##determine if at least one variable has an effect',
'atLeastOne <- ifelse(NmainVarNames > 0, TRUE, FALSE)',
'if(atLeastOne) {',
'##determine number of variables',
'if(NmainVarNames == 1) {',
'effectVariables <- paste("Only", mainVarNames, "influenced the probability of a site to be of aboriginal interest (Table 3). Figure 1 illustrates the effect of this variable.")',
'}',
'',
'if(NmainVarNames == 2) {',
'effectVariables <- paste("A total of ", NmainVarNames, " variables influenced the probability of a site to be of aboriginal interest (Table 3). These variables were ", mainVarNames[-NmainVarNames], " and ", mainVarNames[NmainVarNames], ". Figure 1 illustrates the effect of these ", NmainVarNames, " variables.", sep = "")',
'}',
'if(NmainVarNames > 2) {',
'effectVariables <- paste("A total of ", NmainVarNames, " variables influenced the probability of a site to be of aboriginal interest (Table 3). These variables were ", paste(mainVarNames[-NmainVarNames], collapse = ", "), ", and ", mainVarNames[NmainVarNames], ". Figure 1 illustrates the effect of these ", NmainVarNames, " variables.", sep = "")',
'}',
'} else {',
'effectVariables <- "None of the variables considered were good predictors of the probability of a site to be of aboriginal interest (Table 3)."',
'}',
'```',
'',
'`r effectVariables`',
'',
'```{r results = "asis", echo = FALSE}',
'print(xtable(niceCoef,',
'caption = "Table 3. Model-averaged estimates based on AIC~c~ (regression estimates are weighted by Akaike weights)."), type = "html",',
'include.rownames = TRUE, caption.placement = "top")',
'```',
'',
'',
'```{r fig.cap = "Fig. 1. Effect of variables on probability of a site to be of aboriginal interest. Error bars denote 95% confidence intervals around model-averaged predictions computed for reference level of other variables.", echo = FALSE, eval = atLeastOne}',
'##add figure to plot results',
'##hold constant all variables to their reference level',
'plotVars(mainVarNames,',
          'just.explanatory,',
          'NmainVarNames,',
          'short.list)',
'```',
'',
'<br/>',
'',
'##List of output files created',
'The analysis generated the following files in the following output folder on your computer: ``r getwd( )``.',
'',
'* summary of results: `report.html`',
'* predictions over entire area that matched the values of the explanatory variables used in model building: `predictions.csv`',
'* model selection table: `modelSelection.csv`',
'* model-averaged estimates of explanatory variables: `modavgBetas.csv`',
'')
    writeLines(text = mdownObj, con = "report.Rmd")

    ##convert to html
    render("report.Rmd", output_format = "html_document")

}



###################################
##VIF function
vif <- function(mod) {
  v <- vcov(mod)[-1, -1]
  assign <- attr(model.matrix(mod), "assign")[-1]
  terms <- labels(terms(mod))
  n.terms <- length(terms)
  if (n.terms < 2) {
      vif.vals <- FALSE
      result <- NULL
  } else {
    R <- cov2cor(v)
    detR <- det(R)
    result <- matrix(0, n.terms, 1)
    rownames(result) <- terms
    colnames(result) <- "GVIF"
    for (term in 1:n.terms) {
      subs <- which(assign == term)
      result[term] <- det(as.matrix(R[subs, subs])) *
        det(as.matrix(R[-subs, -subs])) / detR
    }
    
    ##check if any > 10
    vif.vals <- FALSE
    if(any(result > 10)) {vif.vals <- TRUE}
  }
  outVIF <- list("vif.vals" = vif.vals,
                 "result" = result)
  return(outVIF)
}



##add check for Nagelkerke's R2
modelDiag <- function(model.table, model.list) {
    ##model.table is FULL table
    
    ##find null model from ordered table
    ordered.table <- model.table
    topID <- ordered.table[1, ]
    nullID <- ordered.table[ordered.table$K == 1, ]

    ##evidence ratio
    evidence <- topID$AICcWt/nullID$AICcWt
    
    ##find null model among list of models
    resultNull <- model.list[nullID$Modnames]
    ##find top-ranked model among list of models
    resultTop <- model.list[topID$Modnames]

    ##likelihood ratio test statistic
    LR <- -2 * (logLik(resultNull[[1]])[1] - logLik(resultTop[[1]])[1])
    Diff <- attr(logLik(resultTop[[1]]), "df") - attr(logLik(resultNull[[1]]), "df")
    Pval <- 1 - pchisq(q = LR, df = Diff)

    ##Cox and Snell's R2
    n <- length(resultTop[[1]]$fitted)
    CS.R2 <- 1 - exp(-LR/n)

    ##Nagelkerke's R2 (suggests scaling the R2 by using max(R2) for discrete distributions (binomial, Poisson, etc...)
    max.R2 <- (1 - exp(logLik(resultNull[[1]])[1])^(2/n)) #this is the maximum possible value that R2 can attain
    Nagel.R2 <- CS.R2/max.R2

    ##% deviance explained
    null.deviance <- deviance(resultNull[[1]])
    res.deviance <- deviance(resultTop[[1]])
    Deviance2 <- (null.deviance - res.deviance)/null.deviance

    ##organize output
    outList <- list("LR" = LR, "DF" = Diff, "Pvalue" = Pval,
                    "CS.R2" = CS.R2, "Nagel.R2" = Nagel.R2,
                    "DevianceExplained" = Deviance2,
                    "evidenceRatio" = evidence)
    return(outList)
}




plotVars <- function(mainVarNames,
                     just.explanatory,
                     NmainVarNames,
                     short.list) {

    if(length(mainVarNames) == 0) {
        cat("No variable influences the probability of being a site of aboriginal interest and no plot is shown.\n")
    } else {

        ##All variable names
        varNames <- names(just.explanatory)
        ##for each variable, check levels
        varLevels <- lapply(just.explanatory, levels)
        ##extract reference level for each variable
        varRefLev <- lapply(varLevels, FUN = function(i) i[1])
        ##extract number of levels for each variable
        varAllLev <- sapply(varLevels, FUN = length)
        maxLev <- max(varAllLev)
        
        ##create window for each variable
        ##determine number of panels for plot
        if(NmainVarNames == 1) {
            par(mfrow = c(1, 1))}
        if(NmainVarNames == 2) {
            par(mfrow = c(1, 2))}
        if(NmainVarNames == 3) {
            par(mfrow = c(2, 2))}
        if(NmainVarNames == 4) {
            par(mfrow = c(2, 2))}
        if(NmainVarNames == 5) {
            par(mfrow = c(3, 2))}
        if(NmainVarNames == 6) {
            par(mfrow = c(3, 2))}
        if(NmainVarNames == 7) {
            par(mfrow = c(4, 2))}
        if(NmainVarNames == 8) {
            par(mfrow = c(4, 2))}
        
        ##iterate over each variable to identify reference levels
        predPlot <- vector("list", NmainVarNames)
        
        for(k in 1:NmainVarNames) {
            
            ##extract variable name
            varExtr <- mainVarNames[k]
            theLevels <- varLevels[[varExtr]]
            nLevels <- length(theLevels)
            predNames <- data.frame(varRefLev)
            ##repeat nLevels times
            predRep <- do.call("rbind", replicate(nLevels, predNames, simplify = FALSE))
            predRep[, varExtr] <- theLevels
            
            ##combine into character vector
            textCombo <- apply(predRep, MARGIN = 1, FUN = function(x) paste(x, collapse = "."))

            ##model-average predictions
            predPlot[[k]] <- modavgPred(cand.set = short.list, newdata = predRep,
                                        type = "response")

            ##arrange in plot
            xlevs <- 1:nLevels
            xlabs <- theLevels

            plot(predPlot[[k]]$mod.avg.pred ~ xlevs,
                 type = "p", xlab = varExtr,
                 ylab = "Predicted probability",
                 main = "Probability ± 95% CI",
                 xaxt = "n", cex.lab = 1.2,
                 cex.axis = 1.2, cex = 1.2,
                 ylim = c(0, 1))
            
            ##add x axis
            axis(side = 1, at = xlevs,
                 labels = theLevels, cex.axis = 1.2)
            
            ##add error bars
            segments(y0 = predPlot[[k]]$lower.CL, x0 = xlevs,
                     y1 = predPlot[[k]]$upper.CL, x1 = xlevs,)
        }
    }
}

